<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pemesanan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("pemesanan_model");
		$this->load->model("Karyawan_model");
		$this->load->model("menu_model");
	}
	
	public function index()
	{
		$this->listpemesanan();
	}
	
	public function listpemesanan()
	{
		$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$data['data_pemesanan'] = $this->pemesanan_model->tampilDataPemesanan2();
		$this->load->view('home_pemesanan', $data);
	}
	
	public function inputpemesanan($id='')
	{
		$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$data['data_pemesanan'] = $this->pemesanan_model->tampilDataPemesanan2();
		
		if (!empty($_REQUEST)) {
			$m_pemesanan = $this->pemesanan_model;
			$m_pemesanan->savepemesanan($id);
			redirect("pemesanan/listpemesanan", "refresh");
			
		}
		$this->load->view('input_pemesanan', $data);
	}

}
