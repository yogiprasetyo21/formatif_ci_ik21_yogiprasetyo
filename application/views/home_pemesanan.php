
<div style="background:#FFF;color:#0F6;margin:5px 0;padding:10px 0"><marquee direction="left" scrolldelay="20" truespeed="truespeed" scrollamount="1" onmouseover="this.stop()" onmouseout="this.start()">TOKO PIZZA</marquee></div>
 
<head><title>TOKO PIZZA</title></head>
<style>

.toggle, [id^=drop] {
 display: none;
}
.bbt-menu{background:#0F0;width:100%;}
.bbt-menu:after {
    content: '';
    display: table;
    clear: both;
}
nav {
    width:100%;
    padding:0;
}
nav:after {
    content: '';
    display: table;
}
nav ul {
    float:left;
    padding: 0;
    margin: 0;
    list-style: none;
    position: relative;
}
nav ul li {
    margin: 0px;
    display: inline-block;
    float: left;
    background: #0F0; /*** This horizontal menu  background color***/
}
nav ul li ul li{
    background: #CCC; /*** This drop down menu  background color***/
}
nav a, nav a:hover, nav a:visited {
    display: block;
    padding: 0 20px;
    color: #FFF;
    font-size: 18px;
    line-height: 60px;
    text-decoration: none;
}
nav ul li ul li:hover {
    background: #333;
}
nav a:hover {
    background-color: #333;
}
nav ul li ul li a:hover {
    background-color: #111;
}
nav ul ul {
    display: none;
    position: absolute;
    top: 60px; /*** Posisi Down Menu Di Ukur Dari Atas ***/
}
nav ul li:hover > ul {
    display: inherit;
}
nav ul ul li {
    width: 190px; /*** Lebar Down Menu ***/
    float: none;
    display: list-item;
    position: relative;
}
nav ul ul ul li {
    position: relative;
    top: -60px;
    left: 170px;
}
nav label span{
    float:right;
}
.toggle, [id^=drop] {
    display: none;
}
nav input[type=checkbox]{
    display: none;
}
.nav-menu{width:auto;display:block}
.f-search{float:right;width:215.7px;display:block}

.box-search {
background:#EEEEEE;
border: 0;
margin:0;
padding:1.64em 0.5em;
float:left;
}
.btn-search {
background:#333;
border: 0;
color: #FFFFFF;
margin:0;
padding:1.56em 0.5em;
float:left;
cursor:pointer;
}
.btn-search:hover {
background:#000;
}
/* Media Queries
--------------------------------------------- */
@media all and (max-width : 768px) {
.f-search{margin:0 auto;float:none;display:block}
    nav {
        margin: 0;
    }
    .toggle + a,
    .menu {
        display: none;
    }
    .toggle {
        display: block;
        background-color:#333;
        padding: 0 20px;
        color: #FFF;
        font-size: 20px;
        line-height: 60px;
        text-decoration: none;
        border: none;
    }
    .toggle:hover {
        background-color: #000000;
    }
    [id^=drop]:checked + ul {
        display: block;width: 100%;
    }
    nav ul li {
        display: block;
        width: 100%;
    }nav ul ul .toggle,
    nav ul ul a {
        padding: 0 40px;
    }nav ul ul ul a {
        padding: 0 80px;
    }
    nav a:hover,
    nav ul ul ul a {
        background-color: #000000;
    }
    nav ul li ul li .toggle,
    nav ul ul a {
        background-color: #212121;
    }
    nav ul ul {
        float: none;
        position: static;
        color: #ffffff;
    }
    nav ul ul li:hover > ul,
    nav ul li:hover > ul {
        display: none;
    }
    nav ul ul li {
        display: block;
        width: 100%;
    }
    nav ul ul ul li {
        position: static;
    }
}
</style>
<div class='bbt-menu'>
<div class='nav-menu'>
<nav>
  <label for='drop' class='toggle'>Menu <span>&#9776;</span></label>
  <input type='checkbox' id='drop' />
  <ul class='menu'>
    <li><a href='#'>Home</a></li>    
    <li> 
      <!-- First Tier Drop Down -->
      <label for='drop-1' class='toggle'>Data <span>&#9776;</span></label>
      <a href='#'>Data &#9776;</a>
      <input type='checkbox' id='drop-1'/>
      <ul>
        
        <li><a href="<?=base_url();?>karyawan/listkaryawan" style="text-decoration:none;"><b>Data Karyawan</a></li>
        <li><a href="<?=base_url();?>menu/listmenu" style="text-decoration:none;"><b>Data Menu</a></a></li>
        <li><a href="<?=base_url();?>pemesanan/listpemesanan" style="text-decoration:none;"><b>Data Pemesanan</a></li>
        
     
      </ul>
    </li>
    <li>
    <label for='drop-1' class='toggle'>Transaksi <span>&#9776;</span></label>
      <a href='#'>Transaksi &#9776;</a>
       <ul>
         
       </ul>
   <li/>     
    
   
  </ul>
</nav>
</div>
<div class='f-search'><form action='/search' class='search'>
<input class='box-search' name='Search' placeholder='Search...' type='text'>
<input class='btn-search' name='Cari data' type='button' value='Cari data'>
</form></div>
</div>



<table width="100%" height="70" >
  <td align="center"><a href="<?=base_url();?>pemesanan/inputpemesanan" style="text-decoration:none;"><b>INPUT PEMESANAN</a></td>
</table>

 <table width="1156" border="0" bordercolor="#00FF00" cellspacing="0" cellpadding="7" align="center">
  <tr align="center" style="color:#00F" bgcolor="#00FF00" >
  
    <td width="13%" height="47"><b>NIK</b></td>
    <td width="20%"><b>TANGGAL PEMESANAN</b></td>
    <td width="16%"><b>NAMA PELANGGAN</b></td>
	<td width="13%"><b>KODE MENU</b></td>
    <td width="11%"><b>QTY</b></td>
    <td width="13%"><b>TOTAL HARGA</b></td>
    
  </tr>
  <?php
   
      foreach ($data_pemesanan as $data) {
		
?>
  <tr align="center">
 	
    <td><?= $data->nik; ?></td>
    <td><?= $data->tgl_pemesanan; ?></td>
    <td><?= $data->nama_pelanggan; ?></td>
	<td><?= $data->kode_menu; ?></td>
    <td><?= $data->qty; ?></td>
    <td><?= $data->total_harga; ?></td>
    
    
  <?php } ?>
  
</table>

    <p clss="footer">page rendered in <strong>{elapsed_time}</strong> second </p>
    </table>
  
